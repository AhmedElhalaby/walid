<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\VerifyAccounts;
use App\Notifications\VerifyAccount;
use App\Traits\ResponseTrait;
use App\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RegistrationForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return Validator
     */
    protected function getValidatorInstance()
    {
        $data = $this->all();
        if($this->has('mobile') && ($this->mobile == '' || $this->mobile == null)){
            unset($data['mobile']);
        }
        $this->getInputSource()->replace($data);
        /*modify data before send to validator*/
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'password' => 'required|string|min:6',
            'email' => 'required|email|unique:users',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist()
    {
        $user = new User();
        $user->name = $this->name;
        $user->password = Hash::make($this->password);
        $user->email = $this->email;
        if ($this->input('device_token')) {
            $user->device_token = $this->device_token;
            $user->device_type = $this->device_type;
        }

        $user->save();
        Auth::attempt(request(['email', 'password']));
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();
        $user->refresh();
        $user['access_token']= $tokenResult->accessToken;
        $user['token_type']= 'Bearer';

        $code = rand( 10000 , 99999 );
        $token = Str::random(40).time();
        $VerifyAccounts = VerifyAccounts::updateOrCreate(
            ['email' => $this->email],
            [
                'email' => $this->email,
                'code' => $code,
                'token' => $token,
            ]
        );
        $user->notify(
            new VerifyAccount($token,$code)
        );
        return $this->successJsonResponse( [__('messages.saved_successfully')],$user,'User');

    }

}
