<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Traits\ResponseTrait;
use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

class UserRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return Validator
     */
    protected function getValidatorInstance()
    {
        $data = $this->all();
        if($this->has('name') && ($this->name == '' || $this->name == null)){
            unset($data['name']);
        }
        if($this->has('email') && ($this->email == '' || $this->email == null)){
            unset($data['email']);
        }
        if($this->has('app_locale') && ($this->app_locale == '' || $this->app_locale == null)){
            unset($data['app_locale']);
        }
        $this->getInputSource()->replace($data);
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|max:255,',
            'email' => 'email|min:6|unique:users,id,'. auth()->user()->id,
            'device_token' => 'string|required_with:device',
            'device_type' => 'string|required_with:device_token',
            'app_locale' => 'string|in:ar,en',

        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist()
    {
        $logged = auth()->user();
        $logged->update($this->all());
        $logged->save();
        DB::table('oauth_access_tokens')->where('user_id', $logged->id)->delete();
        $tokenResult = $logged->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();
        $logged['access_token']= $tokenResult->accessToken;
        $logged['token_type']= 'Bearer';
        return $this->successJsonResponse( [__('messages.updated_successful')],$logged,'User');

    }
}
