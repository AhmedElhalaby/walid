<?php

namespace App\Http\Requests\AhmedPanel;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function preset($crud)
    {
        $Object = $crud->getEntity();
        foreach ($crud->getFields() as $field) {
            if ($this->has($field['name'])){
                $Object->{$field['name']} = $this->{$field['name']};
            }
        }
        $Object->save();
        return redirect($crud->getRedirect())->with('status', __('admin.messages.saved_successfully'));
    }
}
