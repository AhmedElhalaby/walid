<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\PushNotifications;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    use ResponseTrait;

    /**
     * @return JsonResponse
     */
    public function install(){
        $data = [];
        return $this->successJsonResponse([],$data,'data');
    }

    /**
     * @param $device_token
     * @return string
     */
    public function sendNotification($device_token){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.development.push.apple.com/3/device/'.$device_token);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"callerID\":\"remote@example.com\"}");
        curl_setopt($ch, CURLOPT_SSLCERT, 'Certificates.pem');
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, '1234');

        $headers = array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            return 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        return $result;
//        return PushNotifications::iOS(['mtitle'=>'Hi','mdesc'=>'Test Notification'],$device_token);
    }
    public function IOSNotification(Request $request){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.development.push.apple.com/3/device/'.$request->device_token);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"callerID\":\"remote@example.com\" ,\"room\":\ ".$request->room_name . "}");
        curl_setopt($ch, CURLOPT_SSLCERT, 'Certificates.pem');
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, '1234');


        $headers = array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            return 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        return $result;
    }
}
