<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\ForgetPasswordRequest;
use App\Http\Requests\Api\Auth\LoginForm;
use App\Http\Requests\Api\Auth\PasswordRequest;
use App\Http\Requests\Api\Auth\RefreshForm;
use App\Http\Requests\Api\Auth\RegistrationForm;
use App\Http\Requests\Api\Auth\ResendVerifyForm;
use App\Http\Requests\Api\Auth\ResetPasswordRequest;
use App\Http\Requests\Api\Auth\SocialLoginForm;
use App\Http\Requests\Api\Auth\UserRequest;
use App\Http\Requests\Api\Auth\VerifyForm;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    use ResponseTrait;
    /**
     * Create user
     *
     * @param RegistrationForm $form
     * @return JsonResponse
     */
    public function register(RegistrationForm $form)
    {
        return $form->persist();
    }

    /**
     * Login user and create token
     *
     * @param LoginForm $form
     * @return JsonResponse
     */
    public function login(LoginForm $form)
    {
        return $form->persist();
    }

    /**
     * Login user and create token
     *
     * @param SocialLoginForm $form
     * @return JsonResponse
     */
    public function social_login(SocialLoginForm $form)
    {
        return $form->persist();
    }

    /**
     * Show user profile
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function show(Request $request)
    {
        $user = $request->user();
        $user['access_token']= $request->bearerToken();
        $user['token_type']= 'Bearer';
        return $this->successJsonResponse([],$user,'User');
    }

    /**
     * Logout user (Revoke the token)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request)
    {
        $request->user()->update(['device_token'=>null,'device_type'=>null]);
        $request->user()->token()->revoke();
        $request->user()->token()->delete();
        return $this->successJsonResponse([__('auth.logout')]);
    }

    /**
     * Update user profile
     *
     * @param UserRequest $request
     * @return JsonResponse
     */
    public function update(UserRequest $request)
    {
        return $request->persist();
    }

    /**
     * verify account
     *
     * @param ResendVerifyForm $request
     * @return JsonResponse
     */
    public function resend_verify(ResendVerifyForm $request){
        return $request->persist();
    }

    /**
     * verify account
     *
     * @param VerifyForm $request
     * @return JsonResponse
     */
    public function verify(VerifyForm $request){
        return $request->persist();
    }

    /**
     * Refresh device token
     *
     * @param RefreshForm $request
     * @return JsonResponse
     */
    public function refresh(RefreshForm $request){
         return $request->persist();
    }

    /**
     * @param PasswordRequest $request
     * @return JsonResponse
     */
    public function change_password(PasswordRequest $request){
        return $request->persist();
    }

    /**
     * @param ForgetPasswordRequest $request
     * @return JsonResponse
     */
    public function forget_password(ForgetPasswordRequest $request){
        return $request->persist();
    }

    /**
     * @param ResetPasswordRequest $request
     * @return JsonResponse
     */
    public function reset_password(ResetPasswordRequest $request){
        return $request->persist();
    }
}
