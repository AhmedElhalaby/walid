<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VerifyAccounts extends Model
{
    protected $table = 'verify_accounts';
    protected $fillable = ['email','token','code'];
}
